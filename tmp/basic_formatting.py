# script: basic_formatting.py
# The rest of the line after a hash is a comment
# This illustrates basic Python formatting
a = 1 # the EOL serves as a default delimiter
b = 2; c = 3; # the semicolon is the explicit delimiter

for i in range(3) : # the colon signifies that a suite follows
    print(i) # equally indented statements belong to the same block
    b += i
    c = i*i

    if c < 5 :
        print(c, b) # nested block

print(c, b) # 