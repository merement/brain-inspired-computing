# script: basic_formatting.py
# The rest of the line after the hash is a comment
# This illustrates basic Python formatting
a = 1 # the EOL serves as a default delimiter
b = 2; c = 3; # the semicolon is the explicit delimiter

for i in range(5) : # the colon signifies that a block must be expected
    print(i) # equally idented statements belong to the same block
    b += i
    c = i*i

    if c < 10 :
        print(c, b)
