# FNN_motive
#
# Illustration of basic properties of Fourier NN
#

import numpy as np
import matplotlib.pyplot as plt


# The successive approximation part

def f(x) :
	# function to be represented

	body = np.exp(-x**2*np.sin(np.pi*x)/2)*(0.2*x + 0.43)
	return body

def fN(c, k, x) :
	# Fourier approximation
	# c is the array of coefficients
	# k is the array of "wavevectors"

	if len(c) != len(k) :
		raise ValueError("Coefficients and wavevectors must have the same length")

	body = np.zeros(np.shape(x), dtype=complex)
	for ii in range(len(c)) :
		body += c[ii]*np.exp(1j*k[ii]*x)
	return body

def trainFNN(xtrain, ytrain, kran) :
	# evaluate amplitudes of the Fourier harmonics assuming the canonical distribution of k's
	# within the canoncal distribution 
	# k[j] = 2 pi j/L
	# Hence L = 2 pi/kran[1]

	Nk = len(kran)
	if Nk < 2:
		raise ValueError("At least one harmonic is required")

	Npoints = len(xtrain)

	coefs = np.zeros(np.shape(kran))
	for ii in range(Nk) :
		phase = kran[ii]*xtrain
		coefs[ii] = sum(ytrain*np.cos(phase))/Npoints
	return coefs

# The actual range is known only approximately and needs to be estimated from above

Lest = 2.0

Ntest = 180
xtest = np.linspace(0, Lest, num=Ntest) - 0*Lest/2

Ntrain = 175 # the number of points in the "training" set
xtrain = np.random.rand(Ntrain)*Lest - 0*Lest/2

xtrain = xtest

ytrain = f(xtrain)

Nkran = range(2, Ntrain - 0)

for Nklim in Nkran :
	kkran = np.arange(Nklim)*(np.pi/Lest)

	cc = trainFNN(xtrain, ytrain, kkran)
	print('For ', Nklim, ' harmonics the coefficients are ', cc)
	plt.plot(xtrain, ytrain, '.', xtest, fN(cc, kkran, xtest))


plt.show()