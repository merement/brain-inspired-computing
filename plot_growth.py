# plots data supplied in top-500.csv
#
# The format of the file
#
# Date	First	Last	Sum
#
# 1993-06-01	59.7	0.42	1122.85
# 1993-11-01	124	0.47	1493.35
# ...
#
# So that columns contain
# 1. Date in the %Y - %m - %d format
# 2. Floating point of the top 1 computer
# 3. Float of the top 500 computer
# 4. Float of the sum of all top 500 computers

import numpy as np 
import matplotlib.pyplot as plt
# import time

from matplotlib.dates import datestr2num as d2n

col_names = ["Date", "First", "Last", "Sum"]
dtypes = ["object", "float", "float", "float"]
#a, b = np.genfromtxt("top-500-1.csv", converters = { 0: lambda s: d2n(s)}, skip_header = 1, usecols=(0, 3) )

data = np.genfromtxt("top-500.csv", skip_header = 1, names = col_names, dtype=dtypes )

dates = [d2n(i.decode("utf-8")) for i in data["Date"]]

plt.plot_date( x = dates, y = data["Sum"]/1e4)
plt.yscale("log")
plt.title('Top-500 Performance')
plt.xlabel('Date')
plt.ylabel('GFLOPS')

plt.savefig("top-500.png", dpi = 300, bbox_inches="tight")
plt.show()